import { defineStore } from 'pinia';
import { $getPosts, $addPost, $updatePost, $deletePost } from '../services/index';
import { IPost, IBodyPost } from '../types/index';

export const usePosts = defineStore('posts', {
  state: () => {
    return {
      posts: [] as IPost[],
      error: false
    };
  },
  getters: {
    getPosts(state) {
      return state.posts;
    }
  },
  actions: {
    async fetchPosts() {
      try {
        const { data } = await $getPosts();
        this.posts = data;
      } catch (error) {
        this.error = true;
        console.log('error while fetch');
        this.error = false;
      }
    },
    async updatePost(postData: IBodyPost, id: number) {
      try {
        const { data } = await $updatePost(id, postData);
        this.posts.forEach(post => {
          if (post.id === id) {
            post.title = data.title;
            post.body = data.body;
          }
        });


      } catch (error) {
        this.error = true;
        console.log('error while update post');
        this.error = false;
      }
    },
    async deletePost(id: number) {
      try {
        await $deletePost(id);
      } catch (error) {
        this.error = true;
        console.log(error);
        console.log('error while delete post');
        this.error = false;
      }
      this.posts = this.posts.filter((post) => post.id !== id);
    },
    async addPost(post: IBodyPost) {
      try {
        const { data } = await $addPost(post);
        this.posts.unshift(data);
      } catch (error) {
        this.error = true;
        console.log('error while add post');
        this.error = false;
      }
    }
  }
});
