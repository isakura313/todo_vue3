import { $axios } from '../$axios';
import { IPost, IPostsData, IPostData, IBodyPost } from '../types/index';

async function $getPosts(): Promise<IPostsData> {
  return await $axios.get('/posts');
}

async function $addPost(post: Partial<IPost>): Promise<IPostData> {
  return await $axios.post(`/posts`, post);
}

async function $deletePost(id: number): Promise<[]> {
  return await $axios.delete(`/posts/${id}`);
}

async function $updatePost(id: number, post: IBodyPost): Promise<IPostData> {
  return await $axios.put(`/posts/${id}`, post);
}

export { $getPosts, $addPost, $updatePost, $deletePost };
