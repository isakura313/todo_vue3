export interface IPost {
  userId?: number;
  id: number;
  title: string;
  body: string;
}
export interface IBodyPost {
  title: string;
  body: string;
}

export interface IPostsData {
  data: IPost[];
}

export interface IPostData {
  data: IPost;
}